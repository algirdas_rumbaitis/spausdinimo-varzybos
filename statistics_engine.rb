require 'yaml'

class StatisticsEngine

  attr_accessor :storageFile, :records
  
	def initialize(storageFile)
		@storageFile = correct_filename(storageFile)
		load_records
	end
	
	def load_records
		if (storage_file_exists)
			@records = YAML.load_file(@storageFile)
		else 
			@records = {}
		end
  end
	
	def correct_filename(filename)
		if (/.*.yml/.match(filename))
			return filename
		else
			return filename + ".yml"
		end
	end	
	
	def add_record(player, textTitle, time)
		if @records[textTitle] == nil
			@records[textTitle] = {}
		end
		@records[textTitle][player] = time
		store_records
	end
		
	def remove_text_records(text)
		@records.delete(text)
		store_records
	end	
	
	def clear_records
		@records.clear
		store_records
	end	
	
	def get_text_records(text)
		return @records[text]
	end		
	
	def get_sorted_text_records(text)
		records = @records[text].sort_by {|_key, value| value}
		return records
	end
	
	def store_records
		File.open(@storageFile, 'w') do |out|   # To file
			YAML.dump(@records, out)
		end
	end
	
	def storage_file_exists
		return File.exists?(@storageFile)
	end
end
