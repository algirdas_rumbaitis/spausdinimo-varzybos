require_relative 'text_engine'
require_relative 'game_engine'
require_relative 'statistics_engine'

class UserInterface

	def initialize()
	
		text_engine = TextEngine.new("texts")
		text_engine.load_texts()
		
		statistics_engine = StatisticsEngine.new("records")
		statistics_engine.load_records()
		
		game_engine = GameEngine.new()
		
		print_title()
		user_name = get_name
		print_menu()
		user_input = -1

		while (user_input != 5) 

			user_input = gets.chomp.to_i
			
			case user_input
				when 0
					if text_engine.texts.length != 0
						puts "Games is starting"
						puts "Choose a text:"
						text_engine.texts.each_key { |key| puts "	-- " + key }
						textTitle = gets.chomp		
						game_engine.word_array = text_engine.get_text(textTitle)

						next_word = game_engine.give_next_word()
						game_engine.start_timer()
						while (next_word != nil)
							puts "Next word: " + next_word
							user_word = gets.chomp
							game_engine.receive_next_word(user_word)
							next_word = game_engine.give_next_word() 
						end
						game_engine.end_timer()
						time = game_engine.time_absolute
						accuracy = game_engine.calculate_accuracy
						vmp = game_engine.calculate_WPM()
						puts "Your time:" + time.to_s
						puts "Your accuracy:" + accuracy.to_s
						puts "Your WMP: " + vmp.to_s
								
						statistics_engine.add_record(user_name, textTitle, time)
						
						
						game_engine.reset		
						print_menu()
					else
						puts "Currently texts database is empty"
					end
				when 1
					puts "--Available texts:"
					text_engine.texts.each_key { |key| puts "	-- " + key }
				when 2
					puts "--Type in name:"
					textTitle = gets.chomp
					puts "--Type in word array"
					text = gets.chomp
					text_engine.add_text(textTitle, text)
				when 3
					puts "--Enter the title of the text you want to remove"
					textTitle = gets.chomp
					text_engine.remove_text(textTitle)
				when 4
					puts "Choose a text:"
					text_engine.texts.each_key { |key| puts "	-- " + key }
					textTitle = gets.chomp
					records = statistics_engine.get_text_records(textTitle);
					if records != nil
						puts "Records for " + textTitle
						statistics_engine.get_sorted_text_records(textTitle).each { |player, time| puts player + ": " + time.to_s }
					else 
						puts "There are no records for this text yet!"
					end
				when 5
					puts "--Thank you for playing. Shutting down"
				else
					puts "--Wrong input. Please choose one of the following"
					print_menu()
			end
		end
	end

	
	
	def print_title()
		puts "	------------------- SPAUSDINIMO VARZYBOS ---------------- "
	end

	def print_menu()
		puts "		---------------------------------------------"
		puts "		[0] Play"
		puts "		[1] List available texts"
		puts "		[2] Add a text"
		puts "		[3] Remove a text"
		puts "		[4] View scoreboard"
		puts "		[5] Quit"
		puts "		---------------------------------------------"
	end

	def get_name()
		puts "Please enter your name:"
		return gets.chomp

	end
end

userInterface = UserInterface.new()