require 'yaml'
require 'pathname'

class TextEngine

  attr_accessor :storageFile, :texts
  
	def initialize(storageFile)
		@storageFile = correct_filename(storageFile)
		load_texts
	end
	
	def storage_file_exists
		return File.exists?(@storageFile)
	end	
	
	def correct_filename(filename)
		if (/.*.yml/.match(filename))
			return filename
		else
			return filename + ".yml"
		end
	end		
	
	def load_texts
		if (storage_file_exists)
			@texts = YAML.load_file(@storageFile)
		else 
			@texts = {}
		end
  end
	
	def add_text(textTitle, text)
		@texts[textTitle] = text.split(' ')
		store_texts
	end
	
	def delete_repetitive_words(textTitle)
		text = @texts[textTitle]
		text.uniq!
		if text != nil
			@texts[textTitle] = text
		end
	end
		
	def remove_text(textTitle)
		@texts.delete(textTitle)
		store_texts
	end	
	
	def clear_texts
		@texts.clear
		store_texts
	end	
	
	def get_text(textTitle)
		return @texts[textTitle]
	end		
	
	def store_texts
		File.open(@storageFile, 'w') do |out|   # To file
			YAML.dump(@texts, out)
		end
	end
end
