require 'spec_helper'
 
describe GameEngine do

	before do
		@game_engine = GameEngine.new()
	end
	
	describe '#initialize' do
	
		context 'when initialized' do
			it 'is a correct instance' do
				expect(@game_engine).to be_an_instance_of GameEngine
			end
	
			it 'does not have a loaded word array' do
				expect(@game_engine.word_array).to be_empty
			end
			
			it 'sets its mistakes counter to 0' do
				expect(@game_engine.mistakes).to equal(0)
			end
			
			it 'sets its word counter to 0' do
				expect(@game_engine.counter).to equal(0)
			end			
		
			it 'sets its characters counter to 0' do
				expect(@game_engine.counter).to equal(0)
			end			
			
		end
		
	end
		
	describe '#start_timer' do
	
		context 'when called' do
		
			it "captures current system time" do
				@game_engine.start_timer
				expect(@game_engine.time_start).to be_an_instance_of Time
			end			

		end
		
	end		
	
	describe '#end_timer' do
	
		context 'when called after the timer has started' do
		
			it "calculates the time it took to complete tasks" do
				@game_engine.start_timer
				@game_engine.end_timer
				expect(@game_engine.time_absolute).to_not be_nil
			end

		end
		
		context 'when called before the timer has started' do
		
			it "returns nil" do
				@game_engine.end_timer
				expect(@game_engine.time_absolute).to be_nil
			end

		end		
		
	end		
	
	
	describe '#give_next_word' do
	
		context 'when no text is loaded' do
		
			it "returns nil" do
				expect(@game_engine.give_next_word).to be_nil
			end

		end
		
		context 'when text is loaded and there are still words left' do
		
			before do
				@test_text = %w(foo bar)
				@game_engine.word_array = @test_text
			end
		
			it "returns the following word" do
				expect(@game_engine.give_next_word).to equal(@test_text[0])
			end
			
			it "increases the counter by one" do
				@game_engine.give_next_word	
				expect(lambda {
				@game_engine.give_next_word
				}).to change(@game_engine, :counter).by(1)
			end

		end		

		context 'when text is loaded and there are no more words left' do
		
			before do
				@test_text = []
				@game_engine.word_array = @test_text
			end
		
			it "returns nil" do
				expect(@game_engine.give_next_word).to be_nil
			end

		end	
		
	end			
	
	describe '#receive_next_word' do
	
		context 'when no text is loaded' do
		
			it "returns nil" do
				expect(@game_engine.receive_next_word("test")).to be_nil
			end

		end
		
		context 'when text is loaded and a correct answer is given' do
		
			before do
				test_text = %w(foo bar)
				@game_engine.word_array = test_text
			end
		
			it "accept a word from user and returns true" do
				next_word = @game_engine.give_next_word
				expect(@game_engine.receive_next_word(next_word)).to be_true
			end

		end		

		context 'when text is loaded and an incorrect answer is given' do
		
			before do
				test_text = %w(foo bar)
				@game_engine.word_array = test_text
			end
			
			it "accept a word from user and returns false" do
				next_word = @game_engine.give_next_word
				expect(@game_engine.receive_next_word("false")).to be_false
			end
			
			it "decreases words counter by one" do
				@game_engine.give_next_word	
				expect(lambda {
				@game_engine.receive_next_word("false")
				}).to change(@game_engine, :counter).by(-1)
			end

			it "increases mistakes counter by one" do
				@game_engine.give_next_word	
				expect(lambda {
				@game_engine.receive_next_word("!correct")
				}).to change(@game_engine, :mistakes).by(1)
			end			
			
		end				
			
		context 'when text is loaded and there are no more words left' do
		
			before do
				test_text = ["test"]
				@game_engine.word_array = test_text
			end
		
			it "returns nil" do
				@game_engine.give_next_word
				@game_engine.give_next_word
				expect(@game_engine.receive_next_word("test")).to be_nil
			end

		end	
		
	end			
	
	describe '#reset' do
		
		context 'when reset' do
		
		before do
				test_text = %w(foo bar)
				@game_engine.word_array = test_text
				@game_engine.give_next_word
				@game_engine.reset
			end
		
			it 'sets its mistakes counter to 0' do
				expect(@game_engine.mistakes).to equal(0)
			end
			
			it 'sets its word counter to 0' do
				expect(@game_engine.counter).to equal(0)
			end			
		
			it 'sets its characters counter to 0' do
				expect(@game_engine.counter).to equal(0)
			end			
			
		end
		
	end	
	
	describe '#calculate_accuracy' do
		
		context 'when called while word counter < 1' do
				
			it 'returns nill' do
				expect(@game_engine.calculate_accuracy).to be_nil
			end
		
		end

		context 'when called while word counter > 1' do
		
			before do
				@test_text = %w(foo bar test)
				@game_engine.word_array = @test_text
			end
		
			it 'calculates accuracy' do
				next_word = @game_engine.give_next_word
				@game_engine.receive_next_word(next_word)
				@game_engine.give_next_word
				@game_engine.receive_next_word("!correct")
				@game_engine.give_next_word
				expect(@game_engine.calculate_accuracy).to equal(0.5)
			end
		
		end
		
	end		

	describe '#calculate_WPM' do
		
		context 'when absolute time is not calculated' do
				
			it 'returns nill' do
				expect(@game_engine.calculate_WPM).to be_nil
			end
		
		end

		context 'when absolute time is calculated' do
		
			before do
				@test_text = %w(foo bar test)
				@game_engine.word_array = @test_text
				
				
			end
		
			it 'calculates WPM' do
				next_word = @game_engine.give_next_word
				@game_engine.receive_next_word(next_word)
				next_word = @game_engine.give_next_word
				@game_engine.receive_next_word(next_word)
				@game_engine.time_absolute = 0.1	
				expect(@game_engine.calculate_WPM).to equal(720)
			end
		
		end
		
	end	
	
end
