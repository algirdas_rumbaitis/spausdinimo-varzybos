require 'simplecov'
SimpleCov.start

require_relative '../game_engine'
require_relative '../text_engine'
require_relative '../statistics_engine'

Dir[File.dirname(__FILE__) + "/support/**/*.rb"].each {|f| require f}