require 'spec_helper'
 
describe StatisticsEngine do

	before do
		@test_file = "records_test.yml"
		@stats_engine = StatisticsEngine.new(@test_file)
	end
	
	describe '#initialize' do
	
		context 'when initialized' do
		
			it 'is a correct instance' do
				expect(@stats_engine).to be_an_instance_of StatisticsEngine
			end	
			
		end
		
	end	
	
	describe '#storage_file_exists' do
	
		context 'when storage file does not exists' do
		
			it 'returns false' do
					@stats_engine.storageFile = "doesnt_exist.yml"
					expect(@stats_engine.storage_file_exists).to be_false
			end	
			
		end
		
	end			

	describe '#correct_filename' do
	
		context 'when filename does not have an .yml ending' do
		
			it 'adds the .yml ending' do
				string = "test"
				expect(@stats_engine.correct_filename(string)).to match(".*.yml")
			end	
			
		end
		
		context 'when filename has an .yml ending' do
		
			it 'return the same string' do
				string = "test.yml"
				expect(@stats_engine.correct_filename(string)).to equal(string)
			end	
			
		end		
	end			
	
	describe '#load_records' do
	
		context 'when storage file exists' do
		
			it 'loads texts from the file' do
				expect(@stats_engine.records).to_not be_nil
			end	
			
		end
		
		context 'when storage file does not exist' do
		
			it 'creates a new empty array' do
				@stats_engine.storageFile = "doesnt_exist.yml"
				@stats_engine.load_records()
				expect(@stats_engine.records).to eq({})
			end	
			
		end		
		
	end			
	
	describe '#add_record' do
	
		before do
			@player = "test player"
			@text_title = "Test 1"
			@time = 1
		end	
	
		context 'when no records of given text yet exist' do
		
			it 'creates an empty hash of given text' do
				@stats_engine.clear_records
				@stats_engine.add_record(@player,@text_title, @time)
				expect(@stats_engine.records[@text_title]).to include(@player)
			end	
			
			it 'adds the record into the hash' do
				@stats_engine.add_record(@player,@text_title, @time)
				expect(@stats_engine.records[@text_title]).to include(@player)
			end				
			
		end		
		
		context 'when other records of given text exist' do
			
			it 'adds the record into the hash' do
				@stats_engine.add_record("test player",@text_title, 1)
				@stats_engine.add_record(@player,@text_title, @time)
				expect(@stats_engine.records[@text_title]).to include(@player)
			end				
			
		end				
			
	end			
	
	describe '#remove_text_records' do
	
		context 'when user removes a text' do
		
			it 'removes it from records hash' do
				player = "test player"
				text_title = "Test 1"
				time = 1
				@stats_engine.add_record(player,text_title, time)
				@stats_engine.remove_text_records(text_title)		
				expect(@stats_engine.records).to be_empty
			end	
		
		end		
		
	end			
	
	describe '#clear_records' do
	
		context 'when clears all records' do
		
			it 'removes everything from records hash' do
				player = "test player"
				text_title = "Test 1"
				time = 1
				@stats_engine.add_record(player,text_title, time)
				@stats_engine.remove_text_records(text_title)		
				expect(@stats_engine.records).to be_empty
			end	
		
		end		
		
	end			
	
	describe '#get_text_records' do
	
		context 'when user wants to get all text records' do
		
			it 'returns the queried text records from records hash' do
				player = "test player"
				text_title = "Test 1"
				time = 1
				@stats_engine.add_record(player,text_title, time)
				expect(@stats_engine.get_text_records(text_title)).to eq({player => time})
			end	
		
		end		
		
	end			
	
	describe '#get_sorted_text_records' do
	
		context 'when user wants to get a sorted list of text records' do
		
			it 'returns the queried text sorted records from records hash' do
				player = "test player"
				player2 = "test player 2"
				player3 = "test player 3"
				text_title = "Test 1"
				time = 4
				time2 = 1		
				time3 = 5		
				@stats_engine.add_record(player,text_title, time)
				@stats_engine.add_record(player2,text_title, time2)
				@stats_engine.add_record(player3,text_title, time3)
				expect(@stats_engine.get_sorted_text_records(text_title)).to be_sorted
			end	
		
		end		
		
	end		
	
	describe '#store_records' do
	
		context 'when user wants to store records' do
		
			it 'dumps texts to the storage file' do
				@stats_engine.store_records
				expect(@stats_engine.storage_file_exists).to be_true
			end	
		
		end		
		
	end
	
	
	
end
