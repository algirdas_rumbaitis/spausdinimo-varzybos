require 'spec_helper'
 
describe TextEngine do

	before do
		@test_file = "texts_test.yml"
		@text_engine = TextEngine.new(@test_file)
	end
	
	describe '#initialize' do
	
		context 'when initialized' do
		
			it 'is a correct instance' do
				expect(@text_engine).to be_an_instance_of TextEngine
			end	
			
		end
		
	end	
	
	describe '#storage_file_exists' do
	
		context 'when storage file does not exists' do
		
			it 'returns false' do
					@text_engine.storageFile = "doesnt_exist.yml"
					expect(@text_engine.storage_file_exists).to be_false
			end	
			
		end
		
	end			

	describe '#correct_filename' do
	
		context 'when filename does not have an .yml ending' do
		
			it 'adds the .yml ending' do
				string = "test"
				expect(@text_engine.correct_filename(string)).to match(".*.yml")
			end	
			
		end
		
		context 'when filename has an .yml ending' do
		
			it 'return the same string' do
				string = "test.yml"
				expect(@text_engine.correct_filename(string)).to equal(string)
			end	
			
		end		
	end			
	
	describe '#load_texts' do
	
		context 'when storage file exists' do
		
			it 'loads texts from the file' do
				expect(@text_engine.texts).to_not be_nil
			end	
			
		end
		
		context 'when storage file does not exist' do
		
			it 'creates a new empty array' do
				@text_engine.storageFile = "doesnt_exist.yml"
				@text_engine.load_texts()
				expect(@text_engine.texts).to eq({})
			end	
			
		end		
		
	end		
	
	describe '#add_text' do
	
		context 'when user adds a new text' do
		
			before do
				string = "a new text to be added"
				@name = "a new text"
				@text_engine.add_text(@name, string)
			end
		
			it 'adds the new text into texts hash' do
				expect(@text_engine.texts).to include(@name)
			end	
			
			it 'splits the given string into word array' do
				expect(@text_engine.texts[@name]).to be_kind_of(Array);
			end	
									
		end		
		
	end			
	
	describe '#delete_repetitive_words' do
	
		context 'when a text with repetitive words is given' do
		
			it 'removes repetitive words' do
				string = "a a b b not_repetitive"
				name = "repetitive"
				@text_engine.add_text(name, string)
				@text_engine.delete_repetitive_words(name)
				expect(@text_engine.texts[name]).to repeat_words_times(1)
			end	
									
		end		
		
	end				
	
	
	
	
	describe '#remove_text' do
	
		context 'when user removes a text' do
		
			it 'removes it from texts hash' do
				string = "a new text to be added"
				name = "a new text"
				@text_engine.add_text(name, string)
				@text_engine.remove_text(name)		
				expect(@text_engine.texts).to_not include(name)
			end	
		
		end		
		
	end			

	describe '#clear_texts' do
	
		context 'when user clears all texts' do
		
			it 'removes everything from texts hash' do
				string = "a new text to be added"
				name = "a new text"
				name2 = "a second new text"
				@text_engine.add_text(name, string)
				@text_engine.add_text(name2, string)	
				@text_engine.clear_texts
				expect(@text_engine.texts).to be_empty
			end	
		
		end		
		
	end		
	
	describe '#get_text' do
	
		context 'when user wants to get a text' do
		
			it 'returns the queried text from texts hash' do
				string = "test test2"
				name = "a new text"
				@text_engine.add_text(name, string)
				expect(@text_engine.get_text(name)).to eq(["test", "test2"])
			end	
		
		end		
		
	end			
	
	describe '#store_texts' do
	
		context 'when user wants to store texts' do
		
			it 'dumps texts to the storage file' do
				@text_engine.store_texts
				expect(@text_engine.storage_file_exists()).to be_true
			end	
		
		end		
		
	end				
	
end
