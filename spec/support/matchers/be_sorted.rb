RSpec::Matchers.define :be_sorted do
  match do |actual|
		result=true
		record_previous = 0;
    actual.each do
      |player, record|
			result=false if(record < record_previous)
			record_previous = record
			break if(!result)
    end
		result
	end
	
end