class GameEngine
  attr_accessor :word_array, :time_start, :time_finish, :time_absolute, :mistakes
	attr_reader :counter
  
	def initialize
		@counter = 0
		@mistakes = 0
		@characters_typed = 0
		@word_array = {}
	end

	def start_timer
		@time_start = Time.now
	end
	
	def end_timer
		@time_finish = Time.now
		if @time_start != nil
			return @time_absolute = @time_finish - @time_start
		end
	end	
	
	def give_next_word
		next_word = @word_array[@counter]
		if @word_array != nil
			@counter += 1
		end
		
		return next_word
	end
	
	def receive_next_word(word)
		if @word_array.length != 0
			if @counter-1 < @word_array.length
				if word == @word_array[@counter - 1]
					@characters_typed += word.length
					return true
				else
					@counter -= 1							#The word was incorrect, so let's get back to it
					@mistakes += 1
					return false
				end
			else
				return nil;
			end
		else
			return nil;
		end
	end
	
	def reset
		@counter = 0	
		@mistakes = 0
		@characters_typed = 0
	end	
	
	def calculate_accuracy
		if @counter > 1
			return (@counter-1).fdiv(@mistakes+@counter-1)
		else
			return nil
		end
	end	
	
	def calculate_WPM
		if @time_absolute != nil
			cpm = (@characters_typed/@time_absolute) * 60
			wpm = (cpm / 5).round
			return wpm
		else 
			return nil
		end
	end		
	
end
